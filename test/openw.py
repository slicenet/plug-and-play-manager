import json
from plug_and_play.manager import rest_client

base_url = "https://10.0.8.28/api/v1/namespaces/guest/actions/test-package/hello"
package_url = "https://10.0.8.28/api/v1/namespaces/guest/packages/test-package"
user = "23bc46b1-71f6-4ed5-8c54-816aa4f8c502"
password = "123zO3xZCLrMN6v2BKK1dXYFpXlPkccOFqm12CdAsMgRU4VrNZ9lyGVCGuMDGIwP"


def create_package():
    with open('test_package.json', 'r') as jsn:
        test_pkg = json.load(jsn)

    print(json.dumps(test_pkg))
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    cl = rest_client.BaseRestClient()
    res = cl.put(url=package_url, data=json.dumps(test_pkg), headers=headers, user=user, password=password)
    print(res.text)


def create():
    with open('hello.py', 'r') as py:
        hello_func = py.read()

    with open('hello_action.json', 'r') as jsn:
        hello_act = json.load(jsn)

    hello_act['exec']['code'] = hello_func
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    cl = rest_client.BaseRestClient()
    res = cl.put(url=base_url, data=json.dumps(hello_act), headers=headers, user=user, password=password)
    print(res.text)


def get():
    cl = rest_client.BaseRestClient()
    res = cl.get(url=base_url, user=user, password=password)
    print(res.text)


def invoke():
    cl = rest_client.BaseRestClient()
    data = {"name": "scemo"}
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    res = cl.post(url=base_url+"?blocking=true&result=true", data=json.dumps(data), headers=headers, user=user,
                  password=password)
    print(res.text)


def delete():
    cl = rest_client.BaseRestClient()
    res = cl.delete(url=base_url, user=user, password=password)
    print(res.text)


def delete_package():
    cl = rest_client.BaseRestClient()
    res = cl.delete(url=package_url, user=user, password=password)
    print(res.text)


if __name__ == '__main__':
    create_package()
    create()
    get()
    invoke()
    delete()
    delete_package()

