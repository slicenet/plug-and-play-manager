#  Copyright (c)  2019.  Nextworks s.r.l.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#


import logging
import tornado.web
import tornado.ioloop
import tornado.websocket
from manager import exception, lcm

module_name = 'P&P_MANAGER_REST_UI'
logger = logging.getLogger(module_name)

slice_management_base_endpoint = "/plug-and-play-manager/slice/([^/]*)/"
slice_configuration_base_endpoint = "/plug-and-play-manager/slice-config/([^/]*)/"
manager_configuration_base_endpoint = "/plug-and-play-manager/admin/command/"


def make_slice_management_app():
    return tornado.web.Application(
        [
            (r""+slice_management_base_endpoint, SliceManagementHandler),
            (r"" + slice_configuration_base_endpoint, SliceConfigurationHandler),
        ]
    )


def make_manager_configuration_app():
    return tornado.web.Application(
        [
            (r"" + manager_configuration_base_endpoint + "(db-init|set-kpi|set-descriptor|get-db|set-config)",
             ManagerConfigurationHandler),
        ]
    )


class SliceManagementHandler(tornado.web.RequestHandler):

    # Case POST: slice creation request
    def post(self, slice_id):
        try:
            payload = tornado.escape.json_decode(self.request.body)
            lcm.create_slice(slice_id, payload)
        except (exception.PPParserError, exception.PPDbError) as e:
            logger.error('ERROR in parsing slice creation request. ID: {} - {}'.format(slice_id, str(e)))
            self.set_status(500, "INTERNAL ERROR")
        except exception.PPSliceManagementError as e:
            logger.error('ERROR in parsing slice creation request. ID: {} - {}'.format(slice_id, str(e)))
            self.set_status(409, "CONFLICT")

    # Case GET: slice info request
    def get(self):
        pass

    # Case PUT: slice update request
    def put(self):
        pass

    # Case DELETE: slice termination request
    def delete(self, slice_id):
        try:
            lcm.terminate_slice(slice_id)
        except (exception.PPParserError, exception.PPDbError) as e:
            logger.error('ERROR in parsing slice creation request. ID: {} - {}'.format(slice_id, str(e)))
            self.set_status(500, "INTERNAL ERROR")


class SliceConfigurationHandler(tornado.web.RequestHandler):

    # Case POST: slice creation request
    def post(self, slice_id):
        try:
            lcm.configure_slice(slice_id)
        except (exception.PPParserError, exception.PPDbError) as e:
            logger.error('ERROR in parsing slice creation request. ID: {} - {}'.format(slice_id, str(e)))
            self.set_status(500, "INTERNAL ERROR")


class ManagerConfigurationHandler(tornado.web.RequestHandler):

    # case command
    def post(self, command):
        self.set_header("Content-Type", "application/json")
        self.set_header("Access-Control-Allow-Origin", "*")
        try:
            if command == 'set-kpi':
                payload = tornado.escape.json_decode(self.request.body)
                # Set KPI on db
                lcm.set_new_kpi(payload)
                
            elif command == 'db-init':
                lcm.rebuild_db()
            elif command == 'set-config':
                payload = tornado.escape.json_decode(self.request.body)
                lcm.set_configuration(payload)
            else:
                logger.error("COMMAND NOT FOUND: {}".format(command))
                self.set_status(404, "NOT FOUND")
                self.write("COMMAND NOT FOUND: {}".format(command))
        except exception.PPParserError as e:
            logger.error("ERROR in parsing JSON payload - {}".format(str(e)))
            self.set_status(400, "BAD REQUEST")

    def get(self, command):
        try:
            payload = lcm.get_persistent_data(command)
            self.write(payload)
        except (exception.PPParserError, exception.PPDbError) as e:
            logger.error('ERROR in retrieving DB INFORMATION' + str(e))
            self.set_status(500, "INTERNAL ERROR")


def main(slice_port=50001, config_port=50002):
    osa_app = make_slice_management_app()
    osa_app.listen(port=slice_port)

    plugin_app = make_manager_configuration_app()
    plugin_app.listen(port=config_port)
    try:
        print("Starting REST UI")
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        logger.error("Keyboard Interrupt caught: closing.")
    finally:
        exit(0)


if __name__ == '__main__':
    main()
