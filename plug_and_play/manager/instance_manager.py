#  Copyright (c)  2019.  Nextworks s.r.l.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

# P&P Manager IM (Instance Manager)
import logging
from manager import driver, parser, exception

module_name = "P&P_MANAGER_IM"
logger = logging.getLogger(module_name)


class PPInstanceManager:

    def __init__(self, instance_id, instance_name, namespace='default'):
        self.instance_id = instance_id
        self.instance_name = instance_name
        self.deployments = []
        self.services = []
        self.deployment_name = None
        self.deployment_uid = None
        self.service_name = None
        self.service_uid = None
        self.base_url = None
        self.deployment_manager = driver.DeploymentManager()
        self.service_manager = driver.ServiceManager()
        self.instance_configurator = driver.PPInstanceConfigurator()
        self.namespace = namespace

    # Deployment
    def deploy_instance(self, deployment_data):
        try:
            res = self.deployment_manager.create_deployment(deployment_data, self.namespace)
            res = parser.parse_creation_reply(kind='Deployment', json_reply=res)
            self.deployments.append({'name': res['name'], 'uid': res['uid']})

        except (exception.PPMKUbernetesError, KeyError) as e:
            logger.error("Instance Deployment ERROR - " + str(e))
            raise
        return res

    def remove_instance_deployment(self, name):
        try:
            res = self.deployment_manager.delete_deployment(name, self.namespace)
            res = parser.parse_delete_reply('Deployment', json_reply=res)
        except (exception.PPMKUbernetesError, KeyError) as e:
            logger.error("Instance Deployment Removal ERROR - " + str(e))
            raise
        return res

    def remove_all_slice_deployments(self):
        try:
            for deployment in self.deployments:

                res = self.deployment_manager.delete_deployment(deployment['name'], self.namespace)
                res = parser.parse_delete_reply('Deployment', json_reply=res)
        except (exception.PPMKUbernetesError, KeyError) as e:
            logger.error("Instance Deployment Removal ERROR - " + str(e))
            raise

    def update_instance_deployment(self, new_deployment_data):
        try:
            # FIXME: parse res asap
            res = self.deployment_manager.replace_deployment(self.deployment_name, new_deployment_data, self.namespace)
        except exception.PPMKUbernetesError as e:
            logger.error("Instance Deployment Update ERROR - " + str(e))
            raise
        return res

    # Service
    def expose_instance(self, service_data):
        try:
            res = self.service_manager.create_service(service_data, self.namespace)
            res = parser.parse_creation_reply(kind='Service', json_reply=res)
            self.services.append({'name': res['name'], 'uid': res['uid']})

        except (exception.PPMKUbernetesError, KeyError) as e:
            logger.error('Instance Exposing ERROR - ' + str(e))
            raise
        return res

    def unexpose_instance(self, name):
        try:
            res = self.service_manager.delete_service(name, self.namespace)
            res = parser.parse_delete_reply(kind='Service', json_reply=res)
        except (exception.PPMKUbernetesError, KeyError) as e:
            logger.error('Instance removal ERROR - ' + str(e))
            raise
        return res

    def unxepose_all_slice_instances(self):
        try:
            for instance in self.services:
                res = self.service_manager.delete_service(instance['name'], self.namespace)
                res = parser.parse_delete_reply(kind='Service', json_reply=res)
        except (exception.PPMKUbernetesError, KeyError) as e:
            logger.error('Instance removal ERROR - ' + str(e))
            raise

    # Configuration
    def set_slice_view(self, url, slice_view):
        try:
            res = self.instance_configurator.send_slice_view(url, slice_view)
        except exception.PPMNetworkError as e:
            logger.error("Set Instance View ERROR - " + str(e))
            raise
        return res

    def set_repo_info(self, url, repo_info):
        try:
            res = self.instance_configurator.send_registry_config(url, repo_info)
        except exception.PPMNetworkError as e:
            logger.error("Set Repo Info ERROR - " + str(e))
            raise
        return res

    # Info retrieving
    def get_instance_info(self, url, info_type):
        pass

    def get_im_info(self):
        return {'instance_name': self.instance_name, 'instance_id': self.instance_id,
                'deployment_name': self.deployment_name, 'deployment_uuid': self.deployment_uid,
                'service_name': self.deployment_name, 'service_uuid': self.service_uid, 'base_url': self.base_url}


class FaasManager:

    def __init__(self, package_id, namespace='guest', user=None, password=None):
        self.package_id = package_id  # package id --> slice_id
        self.namespace = namespace
        self.actions = []  # action name list
        self.triggers =[]
        self.trigger_regs = []
        self.rules = []
        self.faas = driver.FaasHanldler()
        self.user = user
        self.password = password

    def create_package(self, url, package, headers=None):
        try:
            url = "{}{}/packages/{}".format(url, self.namespace, self.package_id)
            res = self.faas.create_package(url=url, package=package, headers=headers, user=self.user, password=self.password)
        except exception.PPMNetworkError as e:
            logger.error("ERROR in creating OpenWhisk Package {} - {}".format(self.package_id, str(e)))
            raise
        return res

    def delete_package(self, url):
        try:
            for action in self.actions:  # remove all actions belonging to the package
                self.delete_action(url, action)
            url = "{}{}/packages/{}".format(url, self.namespace, self.package_id)
            res = self.faas.delete_package(url=url, user=self.user, password=self.password)
        except exception.PPMNetworkError as e:
            logger.error("ERROR in deleting OpenWhisk Package {} - {}".format(self.package_id, str(e)))
            raise
        return res

    def create_action(self, url, action, headers=None):
        try:
            url = "{}{}/actions/{}/{}".format(url, self.namespace, self.package_id, action['name'])
            res = self.faas.create_action(url=url, action=action, headers=headers, user=self.user, password=self.password)
            self.actions.append(action['name'])
        except exception.PPMNetworkError as e:
            logger.error("ERROR in creating OpenWhisk Action {} - {}".format(action['name'], str(e)))
            raise
        return res

    def get_action_info(self, url, action_name):
        try:
            url = "{}{}/actions/{}".format(url, self.namespace, action_name)
            res = self.faas.get_action(url=url, user=self.user, password=self.password)
        except exception.PPMNetworkError as e:
            logger.error("ERROR in retrieving OpenWhisk Action Info {} - {}".format(action_name, str(e)))
            raise
        return res

    def delete_action(self, url, action_name):
        try:
            url = "{}{}/actions/{}/{}".format(url, self.namespace, self.package_id, action_name)
            res = self.faas.delete_action(url=url, user=self.user, password=self.password)
            self.actions.remove(action_name)
        except exception.PPMNetworkError as e:
            logger.error("ERROR in deleting OpenWhisk Action {} - {}".format(action_name, str(e)))
            raise
        return res

    def create_trigger(self, url, trigger, headers=None):
        try:
            url = "{}{}/triggers/{}".format(url, self.namespace, trigger['name'])
            res = self.faas.create_trigger(url=url, trigger=trigger, headers=headers, user=self.user,
                                          password=self.password)
            self.triggers.append(trigger['name'])
        except exception.PPMNetworkError as e:
            logger.error("ERROR in creating OpenWhisk trigger {} - {}".format(trigger['name'], str(e)))
            raise
        return res

    def register_trigger(self, url, trigger_reg, headers=None):
        try:
            # url = "{}/triggers/{}/{}".format(url, self.namespace, trigger['name'])
            res = self.faas.register_trigger(url=url, trigger_reg=trigger_reg, headers=headers, user=self.user,
                                             password=self.password)
            self.trigger_regs.append(trigger_reg['triggerName'])
        except exception.PPMNetworkError as e:
            logger.error("ERROR in registration of trigger {} in OpenWhisk - {}".format(trigger_reg['triggerName'], str(e)))
            raise
        return res

    def delete_trigger(self, url, trigger_name):
        try:
            url = "{}{}/triggers/{}".format(url, self.namespace, trigger_name)
            res = self.faas.delete_trigger(url=url, user=self.user, password=self.password)
            self.triggers.remove(trigger_name)
            self.trigger_regs.remove(trigger_name)
        except exception.PPMNetworkError as e:
            logger.error("ERROR in deleting OpenWhisk Trigger {} - {}".format(trigger_name, str(e)))
            raise
        return res

    def create_rule(self, url, rule, headers=None):
        try:
            url = "{}{}/rules/{}".format(url, self.namespace, rule['name'])
            res = self.faas.create_rule(url=url, rule=rule, headers=headers, user=self.user,
                                          password=self.password)
            self.triggers.append(rule['name'])
        except exception.PPMNetworkError as e:
            logger.error("ERROR in creating OpenWhisk Rule {} - {}".format(rule['name'], str(e)))
            raise
        return res

    def delete_rule(self, url, rule_name):
        try:
            url = "{}{}/rules/{}/{}".format(url, self.namespace, rule_name)
            res = self.faas.delete_rule(url=url, user=self.user, password=self.password)
            self.triggers.remove(rule_name)
        except exception.PPMNetworkError as e:
            logger.error("ERROR in deletin OpenWhisk Trigger {} - {}".format(rule_name, str(e)))
            raise
        return res

