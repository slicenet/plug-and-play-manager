#  Copyright (c)  2019  Nextworks s.r.l.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#


import json
import logging
from manager import exception, rest_client

from kubernetes import client, config

module_name = 'P&P_MANAGER_DRIVER'
logger = logging.getLogger(module_name)


# KUBERNETES --------------------------------
class ServiceManager:

    def __init__(self):
        config.load_kube_config(config_file='config')
        self.client = client.CoreV1Api()

    def get_pod_list(self, namespace='default'):
        res = self.client.list_namespaced_pod(namespace=namespace)
        return res

    def get_service_list(self, namespace='default'):
        res = self.client.list_namespaced_service(namespace=namespace)
        return res

    def get_service_info(self, name, namespace='default'):
        res = self.client.read_namespaced_service(name=name, namespace=namespace, pretty='true')
        return res

    def create_service(self, service_yaml, namespace='default'):
        try:
            res = self.client.create_namespaced_service(namespace=namespace, body=service_yaml)
        except client.rest.ApiException as e:
            logger.error("Service creation failed - TYPE: {} - REASON: {}".format(exception.kube_exception_parser(e)[0],
                                                                                  exception.kube_exception_parser(e)[1])
                         )  # Note: AlreadyExists, invalid
            raise exception.PPMKUbernetesError(exception.kube_exception_parser(e)[0],
                                               exception.kube_exception_parser(e)[1])
        else:
            return res

    def delete_service(self, name, namespace='default'):
        try:
            res = self.client.delete_namespaced_service(name=name, namespace=namespace)

        except client.rest.ApiException as e:
            logger.error("Service removal failed - TYPE: {} - REASON: {}".format(exception.kube_exception_parser(e)[0],
                                                                                  exception.kube_exception_parser(e)[1])
                         )  # Note: NotFound
            raise exception.PPMKUbernetesError(exception.kube_exception_parser(e)[0],
                                               exception.kube_exception_parser(e)[1])
        else:
            return res


class DeploymentManager:

    def __init__(self):
        config.load_kube_config(config_file='config')
        self.client = client.AppsV1Api()

    def create_deployment(self, deployment_yaml, namespace='default'):
        try:
            res = self.client.create_namespaced_deployment(namespace=namespace, body=deployment_yaml)
        except client.rest.ApiException as e:
            logger.error("Deployment creation failed - TYPE: {} - REASON: {}".format(exception.kube_exception_parser(e)[0],
                                                                                  exception.kube_exception_parser(e)[1])
                         )  # Note: AlreadyExists
            raise exception.PPMKUbernetesError(exception.kube_exception_parser(e)[0],
                                               exception.kube_exception_parser(e)[1])
        else:
            return res

    def replace_deployment(self, name, deployment_yaml, namespace='default'):
        try:
            res = self.client.replace_namespaced_deployment(name=name, body=deployment_yaml, namespace=namespace)
        except client.rest.ApiException as e:
            logger.error("Deployment replacement failed - TYPE: {} - REASON: {}".format(exception.kube_exception_parser(e)[0],
                                                                                  exception.kube_exception_parser(e)[1])
                         )  # Note: NotFound
            raise exception.PPMKUbernetesError(exception.kube_exception_parser(e)[0],
                                               exception.kube_exception_parser(e)[1])
        else:
            return res

    def delete_deployment(self, name, namespace='default'):
        try:
            res = self.client.delete_namespaced_deployment(name=name, body=client.V1DeleteOptions(), namespace=namespace)
        except client.rest.ApiException as e:
            logger.error("Deployment removal failed - TYPE: {} - REASON: {}".format(exception.kube_exception_parser(e)[0],
                                                                                    exception.kube_exception_parser(e)[1])
                         )  # Note: NotFound
            raise exception.PPMKUbernetesError(exception.kube_exception_parser(e)[0],
                                               exception.kube_exception_parser(e)[1])
        else:
            return res

    def get_deployment_list(self, namespace='default'):
        try:
            res = self.client.list_namespaced_deployment(namespace=namespace)
        except client.rest.ApiException as e:
            print(e)
        else:
            return res


# P&P Control Instance ----------------------------------
class PPInstanceConfigurator:
    def __init__(self):
        self.client = rest_client.BaseRestClient()

    def send_registry_config(self, url, config_json):
        try:
            res = self.client.post(url=url, data=json.dumps(config_json))
        except exception.PPMNetworkError as e:
            logger.error("Error in configuring registry information - TYPE: " + str(e.etype) + " - REASON: "
                         + str(e.reason))
            raise
        else:
            return res

    def send_slice_view(self, url, slice_json):
        try:
            res = self.client.post(url=url, data=json.dumps(slice_json))
        except exception.PPMNetworkError as e:
            logger.error("Error in configuring slice information - TYPE: {} - REASON".format(str(e.etype), str(e.reason)))
            raise
        else:
            return res


# FAAS - OpenWhisk
class FaasHanldler:
    def __init__(self):
        self.client = rest_client.BaseRestClient()

    # packages
    def create_package(self, url, package, headers=None, user=None, password=None):
        try:
            if headers is None:
                headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
            res = self.client.put(url=url, data=json.dumps(package), headers=headers, user=user, password=password)
        except exception.PPMNetworkError as e:
            logger.error("Error creating package on OpenWhisk - TYPE: {} - REASON: {}".format(str(e.etype), str(e.reason)))
            raise
        else:
            return res

    def delete_package(self, url, user=None, password=None):
        try:
            res = self.client.delete(url=url, user=user, password=password)
        except exception.PPMNetworkError as e:
            logger.error("Error in deleting action information - TYPE: {} - REASON: {}".format(str(e.etype), str(e.reason)))
            raise
        else:
            return res

    # actions
    def create_action(self, url, action, headers=None, user=None, password=None):
        try:
            if headers is None:
                headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
            res = self.client.put(url=url, data=json.dumps(action), headers=headers, user=user, password=password)
        except exception.PPMNetworkError as e:
            logger.error("Error in creating action on OpenWhisk - TYPE: {} - REASON: {}".format(str(e.etype), str(e.reason)))
            raise
        else:
            return res

    def get_action(self, url, user=None, password=None):
        try:
            res = self.client.get(url=url, user=user, password=password)
        except exception.PPMNetworkError as e:
            logger.error("Error in deleting action information - TYPE: {} - REASON: {}".format(str(e.etype), str(e.reason)))
            raise
        else:
            return res

    def delete_action(self, url, user=None, password=None):
        try:
            res = self.client.delete(url=url, user=user, password=password)
        except exception.PPMNetworkError as e:
            logger.error("Error in deleting action - TYPE: {} - REASON: {}".format(str(e.etype), str(e.reason)))
            raise
        else:
            return res

    def create_trigger(self, url, trigger, headers=None, user=None, password=None):
        try:
            if headers is None:
                headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
            res = self.client.put(url=url, data=json.dumps(trigger), headers=headers, user=user, password=password)
        except exception.PPMNetworkError as e:
            logger.error("Error in creating trigger on OpenWhisk - TYPE: {} - REASON: {}".format(str(e.etype), str(e.reason)))
            raise
        else:
            return res

    def register_trigger(self, url, trigger_reg, headers=None, user=None, password=None):
        try:
            if headers is None:
                headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
            res = self.client.post(url=url, data=json.dumps(trigger_reg), headers=headers, user=user, password=password)
        except exception.PPMNetworkError as e:
            logger.error("Error in creating trigger on OpenWhisk - TYPE: {} - REASON: {}".format(str(e.etype), str(e.reason)))
            raise
        else:
            return res

    def delete_trigger(self, url, user=None, password=None):
        try:
            res = self.client.delete(url=url, user=user, password=password)
        except exception.PPMNetworkError as e:
            logger.error("Error in deleting trigger - TYPE: {} - REASON: {}".format(str(e.etype), str(e.reason)))
            raise
        else:
            return res

    def create_rule(self, url, rule, headers=None, user=None, password=None):
        try:
            if headers is None:
                headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
            res = self.client.put(url=url, data=json.dumps(rule), headers=headers, user=user, password=password)
        except exception.PPMNetworkError as e:
            logger.error("Error in creating rule on OpenWhisk - TYPE: {} - REASON: {}".format(str(e.etype), str(e.reason)))
            raise
        else:
            return res

    def delete_rule(self, url, user=None, password=None):
        try:
            res = self.client.delete(url=url, user=user, password=password)
        except exception.PPMNetworkError as e:
            logger.error("Error in deleting rule - TYPE: {} - REASON: {}".format(str(e.etype), str(e.reason)))
            raise
        else:
            return res


def main():
    dm = DeploymentManager()
    sm = ServiceManager()
    """with open('manager_persistence/configuration_repo/kubernetes/nxw-slice_kube_deployment_component.yaml') as f:
        dm_yaml = yaml.load(f)"""

    """with open('../test/service_slice_nxw.yaml') as f:
        sm_yaml = yaml.load(f)"""

    # res = dm.create_deployment(dm_yaml)
    # res = dm.replace_deployment(name='plug-play-slice-nxw', deployment_yaml=dm_yaml)
    # res = dm.delete_deployment(name="plug-play-slice-nxw")
    res = dm.get_deployment_list()
    # res = sm.create_service(sm_yaml)
    # res = sm.delete_service(name="plug-play-slice-nxw")
    # res = sm.get_pod_list()
    # res = sm.get_service_info('plug-play-slice-nxw')
    print(res)


if __name__ == '__main__':
    main()
