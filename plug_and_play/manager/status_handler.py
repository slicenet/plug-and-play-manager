"""
    This package is just to keep the status of the P&P Manager through a set of singleton classes
    FIXME: Overall fix -> concurrency
"""
#  Copyright (c)  2019.  Nextworks s.r.l.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#


import json
from manager import exception


# Singleton class to be used as metaclass
class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class ServicePortHandler(metaclass=Singleton):
    def __init__(self, init_port_repo=True, start=30001, stop=31000):
        if init_port_repo:
            self.port_repo = [x for x in range(start, stop)]
        self.innerMap = {}

    def get_service_port(self, slice_id):
        port = self.port_repo[0]
        self.port_repo.remove(port)
        self.innerMap[slice_id] = port
        return port

    def get_assigned_port(self, slice_id):

        return self.innerMap[slice_id]

    def release_port(self, slice_id):
        if slice_id in self.innerMap.keys():
            self.port_repo.append(self.innerMap[slice_id])
            self.port_repo.sort()


class ServiceMultiPortHandler(metaclass=Singleton):
    def __init__(self, init_port_repo=True, start=30001, stop=31000):
        if init_port_repo:
            self.port_repo = [x for x in range(start, stop)]
        self.innerMap = {}

    def get_service_ports(self, slice_id, how_many = 1):
        # check for enough free ports
        ports = []
        if how_many <= len(self.port_repo):

            ports = self.port_repo[:how_many]
            self.port_repo = self.port_repo[how_many:]
            self.innerMap[slice_id] = ports

        return ports

    def get_assigned_ports(self, slice_id):

        return self.innerMap[slice_id]

    def release_ports(self, slice_id):
        if slice_id in self.innerMap.keys():
            self.port_repo + self.innerMap[slice_id]
            self.port_repo.sort()


class SlicerManagerRepo(metaclass=Singleton):
    def __init__(self):
        self.innerMap = {}

    def set_slice_instance_manager(self, slice_id, instance_manager):
        if slice_id in self.innerMap.keys():
            raise exception.PPDbError(reason="Instance Manager already exists for slice " + slice_id)
        self.innerMap[slice_id] = instance_manager

    def get_instance_manager(self, slice_id):
        try:
            return self.innerMap[slice_id]
        except KeyError as e:
            raise exception.PPDbError(reason="Instance Manager NOT FOUND for Slice " + slice_id)

    def remove_instance_manager(self, slice_id):
        try:
            del self.innerMap[slice_id]
        except KeyError as e:
            raise exception.PPDbError(reason="Instance Manager NOT FOUND for Slice " + slice_id)

    #  Utility function
    def check_im_existence(self, slice_id):
        if slice_id in self.innerMap.keys():
            return True

        return False

class FaasManagerRepo(metaclass=Singleton):
    def __init__(self):
        self.innerMap = {}

    def set_faas_manager(self, slice_id, faas_manager):
        if slice_id in self.innerMap.keys():
            raise exception.PPDbError(reason="Faas Manager already exists for slice " + slice_id)
        self.innerMap[slice_id] = faas_manager

    def get_faas_manager(self, slice_id):
        try:
            return self.innerMap[slice_id]
        except KeyError as e:
            raise exception.PPDbError(reason="Faas Manager NOT FOUND for Slice " + slice_id)

    def remove_faas_manager(self, slice_id):
        try:
            del self.innerMap[slice_id]
        except KeyError as e:
            raise exception.PPDbError(reason="Faas Manager NOT FOUND for Slice " + slice_id)

    #  Utility function
    def check_fm_existence(self, slice_id):
        if slice_id in self.innerMap.keys():
            return True

        return False


class ManagerConfigStore(metaclass=Singleton):
    def __init__(self, config_file="manager_config.json"):

        with open(config_file, 'r') as cf:
            self.innerConfig = json.load(cf)

    def get_module_config(self, module_name):
        try:
            return self.innerConfig[module_name]
        except KeyError as e:
            raise exception.PPDbError(reason="Configuration NOT FOUND for module {}".format(module_name))
