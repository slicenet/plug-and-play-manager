"""
    Implementation of P&P Manager DB Handler

"""
#  Copyright (c)  2019.  Nextworks s.r.l.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

import logging
import sqlite3
import hashlib
import os
import re
import json

module_name = 'P&P_MANAGER_DB_HANDLER'
logger = logging.getLogger(module_name)


# Decorator function for checking DB existence
def _db_exists(func):

    def wrapper(self, *args, **kwargs):
        if self.db is None:
            logger.error("Error in connection to SQLite: database object not set")
            raise ValueError("Error in connection to SQLite: database object not set")
        return func(self, *args, **kwargs)

    return wrapper


def _iterate(func):

    def iter_wrapper(self, *args, **kwargs):
        pass

    return iter_wrapper


class DBHandler:

    def __init__(self, db_name="catalogue_db", path="manager/manager_persistence/"):
        self.db = sqlite3.connect(path+db_name)

    @_db_exists
    def __enter__(self):
        self.db.__enter__()
        return self.db.cursor()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.db.__exit__(exc_type, exc_val, exc_tb)

    @_db_exists
    def truncate_table(self, table):
        with self as cur:
            cur.execute("DELETE FROM {}".format(table))
            # cur.execute("VACUUM")

    @_db_exists
    def set_kpi(self, kpi_id, kpi_level, kpi_type):
        # generate md5sum
        message = "{}_{}_{}".format(kpi_id, kpi_level, kpi_type)
        m = hashlib.md5(message.encode('utf-8'))
        with self as cur:
            cur.execute("INSERT INTO kpi VALUES (?, ?, ?, ?)", (kpi_id, kpi_level, kpi_type, m.hexdigest()))

    @_db_exists
    def set_element(self, keyword, element_id, element_name, descriptor_file):
        with self as cur:
            cur.execute("INSERT INTO {} VALUES (?, ?, ?)".format(keyword), (element_id, element_name, descriptor_file))

    @_db_exists
    def element_kpi(self, keyword, element_id, kpi_id, kpi_level, kpi_type):
        # generate md5sum
        message = "{}_{}_{}".format(kpi_id, kpi_level, kpi_type)
        m = hashlib.md5(message.encode('utf-8'))
        with self as cur:
            cur.execute("INSERT INTO {}_kpi VALUES (?, ?)".format(keyword), (element_id, m.hexdigest()))

    @_db_exists
    def get_kpi(self):
        with self as cur:
            cur.execute("SELECT * FROM kpi")
            return cur.fetchall()

    @_db_exists
    def get_plugins(self):
        with self as cur:
            cur.execute("SELECT * FROM plugin")
            return cur.fetchall()

    @_db_exists
    def get_components(self):
        with self as cur:
            cur.execute("SELECT * FROM component")
            return cur.fetchall()

    @_db_exists
    def get_deployments(self):
        with self as cur:
            cur.execute("SELECT * FROM deployments")
            return cur.fetchall()

    @_db_exists
    def get_services(self):
        with self as cur:
            cur.execute("SELECT * FROM service")
            return cur.fetchall()

    @_db_exists
    def get_slice_info(self):
        with self as cur:
            cur.execute("SELECT * FROM slice_file")
            return cur.fetchall()

    @_db_exists
    def get_full_db(self):
        content = dict()
        content["KPI"] = self.get_kpi()
        content['PLUGINS'] = self.get_plugins()
        content['COMPONENTS'] = self.get_components()
        content["SLICE_FILE_INFO"] = self.get_slice_info()
        return content

    @_db_exists
    def get_kpi_hash(self, kpi_id, kpi_level, kpi_type):
        with self as cur:
            cur.execute("SELECT kpi_md5 FROM kpi WHERE kpi_id = ? And kpi_level = ? AND kpi_type = ?",
                        (kpi_id, kpi_level, kpi_type,))
            return cur.fetchone()

    @_db_exists
    def get_element_by_kpi(self, kpi_hash, table):
        with self as cur:
            cur.execute("SELECT * FROM {} WHERE kpi_md5 = ?".format(table), (kpi_hash,))
            return cur.fetchone()

    @_db_exists
    def get_element_info(self, element_id, table):
        with self as cur:
            cur.execute("SELECT * FROM {} WHERE {}_id = ?".format(table, table), (element_id,))
            return cur.fetchone()

    @_db_exists
    def read_faas_action(self, action_type):
        with self as cur:
            cur.execute("SELECT * FROM faas_action WHERE action_type= ?", (action_type,))
            return cur.fetchone()


class SliceInfoHandler(DBHandler):
    @_db_exists
    def get_slice_info(self,  slice_id):
        with self as cur:
            cur.execute("SELECT * FROM slice_file WHERE slice_id = ?", (slice_id,))
            return cur.fetchone()

    @_db_exists
    def store_slice_info(self, slice_id, plugin_deployment_file, component_deployment_file, service_file, view_file):
        with self as cur:
            cur.execute("INSERT INTO slice_file(slice_id, slice_plugin_deployment_file, "
                        "slice_component_deployment_file, slice_service_file, slice_view_file) values (?, ?, ?, ?, ?)",
                        (slice_id, plugin_deployment_file, component_deployment_file, service_file, view_file,))

    def update_slice_info(self, slice_id, field, value):
        with self as cur:
            cur.execute("UPDATE slice_file SET {} = ? where slice_id = ?".format(field), (value, slice_id))

    def remove_slice_info(self, slice_id):
        with self as cur:
            cur.execute("DELETE FROM slice_file WHERE slice_id = ?", (slice_id, ))


class PlugAndPlayControlHandler(DBHandler):

    def reading_plugin_set(self, kpi_args):
        kpi_hash_set = []
        plugin_ids = []
        plugin_info_set = []
        for kpi_set in kpi_args:
            res = self.get_kpi_hash(kpi_id=kpi_set['kpi_id'], kpi_level=kpi_set['kpi_level'],
                                    kpi_type=kpi_set['kpi_type'])
            if res is not None:
                kpi_hash_set.append(res[0])
            else:
                pass
                # FIXME: kpi not found should be handled
        for kpi_hash in kpi_hash_set:
            res = self.get_element_by_kpi(table="plugin_kpi", kpi_hash=kpi_hash)
            if res is not None:
                plugin_ids.append(res[0])
            else:
                pass
                # FIXME: plugin not found should be handled
        for plugin_id in plugin_ids:
            res = self.get_element_info(table="plugin", element_id=plugin_id)
            if res is not None and res not in plugin_info_set:
                plugin_info_set.append(res)
            else:
                pass
                # FIXME: plugin_info not found should be handled
        return plugin_info_set

    def add_new_plugin(self, plugin_info, descriptor_file):
        self.set_element(plugin_info[0], plugin_info[1], plugin_info[2], descriptor_file)
        for element in plugin_info[3]:  # kpi_info_list
            self.element_kpi(plugin_info[0], plugin_info[1], element[0], element[1], element[2])


class ComponentsHandler(DBHandler):
    @_db_exists
    def read_component_set(self, kpi_args):
        kpi_hash_set = []
        components_ids = []
        component_info_set = []
        for kpi_set in kpi_args:
            res = self.get_kpi_hash(kpi_id=kpi_set['kpi_id'], kpi_level=kpi_set['kpi_level'],
                                    kpi_type=kpi_set['kpi_type'])
            if res is not None:
                kpi_hash_set.append(res[0])
            else:
                pass
                # FIXME: kpi not found should be handled
        for kpi_hash in kpi_hash_set:
            res = self.get_element_by_kpi(table="component_kpi", kpi_hash=kpi_hash)
            if res is not None:
                components_ids.append(res[0])
            else:
                pass
                # FIXME: plugin not found should be handled
        for plugin_id in components_ids:
            res = self.get_element_info(table="component", element_id=plugin_id)
            if res is not None and res not in component_info_set:
                component_info_set.append(res)
            else:
                pass
                # FIXME: plugin_info not found should be handled
        return component_info_set

    def add_new_component(self, component_info, descriptor_file):
        self.set_element(component_info[0], component_info[1], component_info[2], descriptor_file)
        for element in component_info[3]: # kpi_info_list
            self.element_kpi(component_info[0], component_info[1], element[0], element[1], element[2])


class FaasHandler(DBHandler):
    def read_faas_actions(self, action_type_list):
        action_list = list()
        for action_type in action_type_list:
            res = self.read_faas_action(action_type)
            action_list.append(res)
        return action_list


class KpiHandler(DBHandler):
    @_db_exists
    def add_new_kpi(self, kpi_info):
        self.set_kpi(kpi_info['kpi_id'], kpi_info['kpi_level'], kpi_info['kpi_type'])


class FileHandler:
    def __init__(self, path="manager/manager_persistence/descriptor_catalogue/", pattern='([^/]*)_descriptor.json'):
        self.path = path
        self.pattern = pattern

    def load_descriptors_list(self):
        file_list = [f for f in os.listdir(self.path) if os.path.isfile(os.path.join(self.path, f)) and
                     re.fullmatch(self.pattern, f) is not None and 'core_control' not in f]

        return file_list

    def set_global_manager_conf(self, config):
        with open('manager_config.json', 'w') as config_file:
            json.dump(config, config_file, indent=2)

    def load_slice_info(self, slice_id):
        with open("{}{}.json".format(self.path, slice_id), "r") as f:
            return json.load(f)

def main():

    db = DBHandler(path='/home/pietro/Projects/slicenet/plug-and-play/management/plug_and_play/manager/manager_persistence/')
    res = db.get_plugins()
    print(res)


if __name__ == "__main__":
    main()
