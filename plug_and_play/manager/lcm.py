#  Copyright (c)  2019.  Nextworks s.r.l.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

import logging
import time
from manager import status_handler, parser, instance_manager, slice_config_generator, exception, db_handler

module_name = 'P&P_MANAGER_LCM'
logger = logging.getLogger(module_name)

slice_manager_repo = status_handler.SlicerManagerRepo()
service_port_handler = status_handler.ServicePortHandler()
service_multiport_handler = status_handler.ServiceMultiPortHandler()
configuration_handler = status_handler.ManagerConfigStore()
faas_manager_repo = status_handler.FaasManagerRepo()


def generate_component_deployment(slice_id, kpi_list, parser_conf, yaml_generator, image_pull_secrets):

    ch = db_handler.ComponentsHandler()
    cpsr_conf = configuration_handler.get_module_config('cpsr')
    component_set = ch.read_component_set(kpi_list)
    descriptor_component_set = parser.DescriptorParser.assemble_element_set(descriptors_info=component_set,
                                                                            core_file_name=None,
                                                                            path=parser_conf['descriptor_path'])
    if not descriptor_component_set:
        return [], []

    if cpsr_conf is None and descriptor_component_set:
        raise exception.PPConfigurationError(reason="CPSR configuration not found")

    component_deployment = yaml_generator.generate_deployment_yaml(slice_id,
                                                                   container_type="component",
                                                                   container_info_list=descriptor_component_set,
                                                                   cpsr_info=cpsr_conf,
                                                                   image_pull_secrets=image_pull_secrets)

    return component_deployment, descriptor_component_set


def generate_plugin_deployment(slice_id, kpi_list, parser_conf, yaml_generator, image_pull_secrets):
    pph = db_handler.PlugAndPlayControlHandler()
    plugin_set = pph.reading_plugin_set(kpi_list)
    descriptor_plugin_set = parser.DescriptorParser.assemble_element_set(descriptors_info=plugin_set,
                                                                         core_file_name=parser_conf['core_file_name'],
                                                                         path=parser_conf['descriptor_path'])

    if descriptor_plugin_set is None:
        return None

    plugin_deployment = yaml_generator.generate_deployment_yaml(slice_id,
                                                                container_type="plugin",
                                                                container_info_list=descriptor_plugin_set,
                                                                image_pull_secrets=image_pull_secrets)
    return plugin_deployment


def deploy_faas_actions(slice_id, slice_info):
    faas_api = configuration_handler.get_module_config('faas_api')
    faas_im = instance_manager.FaasManager(slice_id, faas_api['namespace'], faas_api['user'], faas_api['password'])
    faas_gen = slice_config_generator.FaasConfigGenerator(faas_api['base_package_model'], faas_api['base_action_model'], faas_api['base_trigger_model'],
                                                          faas_api['base_trigger_reg_model'], faas_api['base_rule_model'],
                                                          faas_api['code_file_path'])
    fh = db_handler.FaasHandler()
    action_list = fh.read_faas_actions(slice_info['required_faas'])
    if action_list:
        # package = faas_gen.generate_package_json(faas_api['namespace'], slice_id)
        # faas_im.create_package(url="https://{}{}".format(faas_api['faasIP'], faas_api['base_url']), package=package)
        for element in action_list:  # element --> [action_name, action_type, code_file]
            #action = faas_gen.generate_action_json(faas_api['namespace'], element[0], element[2])
            #faas_im.create_action(url="https://{}{}".format(faas_api['faasIP'], faas_api['base_url']), action=action)
            # TODO Create rules and triggers
            trigger = faas_gen.generate_trigger_json(faas_api['namespace'], element[0]+"-trigger")
            faas_im.create_trigger(url="https://{}{}".format(faas_api['faasIP'], faas_api['base_url']), trigger=trigger)
            trigger_reg = faas_gen.register_trigger_json('{}:{}'.format(faas_api['user'], faas_api['password']), faas_api['brokers'],
                                                         element[3], "/{}/{}".format(faas_api['namespace'], trigger['name']))
            #Register trigger (POST)
            faas_im.register_trigger(url="https://{}{}{}".format(faas_api['faasIP'], faas_api['base_url'], faas_api['trigger_reg_url']), trigger_reg=trigger_reg)

            rule = faas_gen.generate_rule_json(element[0]+"-rule", trigger['name'], element[0], faas_api['namespace'])
            faas_im.create_rule(url="https://{}{}".format(faas_api['faasIP'], faas_api['base_url']), rule=rule)

        faas_manager_repo.set_faas_manager(slice_id, faas_im)


def create_slice(slice_id, slice_info):
    # check slice existence
    sh = db_handler.SliceInfoHandler()
    if sh.get_slice_info(slice_id) is not None or slice_manager_repo.check_im_existence(slice_id) is True:
        raise exception.PPSliceManagementError("Slice ID " + slice_id + " already exists")
    try:
        parser_conf = configuration_handler.get_module_config('parser')
        kpi_list = parser.NbiParser.extract_slice_kpis(slice_info=slice_info)
        topology = parser.NbiParser.extract_slice_topology(slice_info=slice_info)
        yg_config = configuration_handler.get_module_config('slice_config_generator')
        yaml_generator = slice_config_generator.KubeConfigGenerator(yg_config['yaml_save_path'],
                                                                    yg_config['json_save_path'],
                                                                    yg_config['base_deployment_model'],
                                                                    yg_config['base_service_model'])

        if kpi_list:
            kube_im = None
            sh.store_slice_info(slice_id, None, None, None, None)
            # Step 1: Components
            component_deployment, descriport_component_set = generate_component_deployment(slice_id, kpi_list, parser_conf, yaml_generator, yg_config['image_pull_secrets'])
            if component_deployment:
                if kube_im is None:
                    kube_im = instance_manager.PPInstanceManager(instance_id=slice_id, instance_name='none')
                    # kube_im.deploy_instance(deployment_data=component_deployment['data'])
                    sh.update_slice_info(slice_id, "slice_component_deployment_file", component_deployment['file'])

            else:
                logger.info("No Components found")
            # Step 2: Plugins
            plugin_deployment = []
            if topology:
                plugin_deployment = generate_plugin_deployment(slice_id, kpi_list, parser_conf, yaml_generator, yg_config['image_pull_secrets'])
                if plugin_deployment:
                    # service = yaml_generator.generate_service_yaml(slice_id, service_port_handler.get_service_port(slice_id))
                    if kube_im is None:
                        kube_im = instance_manager.PPInstanceManager(instance_id=slice_id, instance_name='none')
                    kube_im.deploy_instance(deployment_data=plugin_deployment['data'])

                    sh.update_slice_info(slice_id, "slice_plugin_deployment_file", plugin_deployment['file'])
                    sh.update_slice_info(slice_id, "slice_view_file", '{}-slice-view.json'.format(slice_id))
                else:
                    logger.info("No Plugins found")

            core = False
            how_many_ports = len(descriport_component_set)
            if plugin_deployment:
                core = True
                how_many_ports = how_many_ports + 1

            ports = service_multiport_handler.get_service_ports(slice_id, how_many_ports)
            if ports:
                if component_deployment:
                    if how_many_ports > len(descriport_component_set):
                        component_deployment['data'] = yaml_generator.modify_component_deployment(slice_id, component_deployment['data'], ports[1:])
                    else:
                        component_deployment['data'] = yaml_generator.modify_component_deployment(slice_id, component_deployment['data'], ports)

                    kube_im.deploy_instance(deployment_data=component_deployment['data'])
                service = yaml_generator.generate_slice_service(slice_id, core, descriport_component_set, ports)
                kube_im.expose_instance(service_data=service['data'])

        else:
            logger.info("No Topology info found")
            # Step 8: Faas
        if slice_info['required_faas'] is not None:
            deploy_faas_actions(slice_id, slice_info)
        else:
            logger.info("No FaaS actions required")
        # Step 4: Store IM and Slice deployment/exposure info
        if kube_im is not None:
            slice_manager_repo.set_slice_instance_manager(slice_id, kube_im)

        else:
            sh.remove_slice_info(slice_id)
    except exception.PPParserError as e:
        logger.error('ERROR in parsing slice creation request. ID: ' + slice_id + ' - ' + str(e))
        raise
    except exception.PPDbError as e:
        logger.error("DB DB - " + str(e))
        raise
    except exception.PPConfigurationError as e:
        logger.error(str(e))
        raise


def configure_slice(slice_id):
    sh = db_handler.SliceInfoHandler()
    if sh.get_slice_info(slice_id) is None and slice_manager_repo.check_im_existence(slice_id) is False:
        raise exception.PPSliceManagementError("Slice ID " + slice_id + "not exists")
    yg_config = configuration_handler.get_module_config('slice_config_generator')
    cpsr_conf = configuration_handler.get_module_config('cpsr')
    config_generator = slice_config_generator.KubeConfigGenerator(yg_config['yaml_save_path'],
                                                                  yg_config['json_save_path'],
                                                                  yg_config['base_deployment_model'],
                                                                  yg_config['base_service_model'])

    # Step 5: Generate Slice view
    parser_conf = configuration_handler.get_module_config('slice_config_generator')
    fh = db_handler.FileHandler(path=parser_conf['sso_slice_path'])
    slice_info = fh.load_slice_info(slice_id)
    kpi_list = parser.NbiParser.extract_slice_kpis(slice_info=slice_info)
    pph = db_handler.PlugAndPlayControlHandler()
    plugin_set = pph.reading_plugin_set(kpi_list)
    parsed_elements = list()
    """for plugin in plugin_set:  # plugin[2] == plugin_descriptor_file
        parsed_element = parser.DescriptorParser.parse_file_descriptor(filename=plugin[2],
                                                                       path=parser_conf['descriptor_path'])
        parsed_elements.append(parsed_element)
    """
    slice_view = config_generator.generate_slice_view(slice_id, slice_info, parsed_elements)
    repo_info = config_generator.generate_repo_info(cpsr_conf['cpsrIP'], cpsr_conf['cpsrPort'])
    # Step 6: Push Slice view (?) NOTE: Not sure if push config to app here
    node_info = configuration_handler.get_module_config('deployment_node')
    endpoints = configuration_handler.get_module_config('core_endpoints')
    service_port = service_port_handler.get_assigned_port(slice_id)

    imanager = slice_manager_repo.get_instance_manager(slice_id)

    imanager.set_repo_info('http://{}:{}{}'.format(node_info['ip'], service_port, endpoints['cpsr']), repo_info)

    imanager.set_slice_view('http://{}:{}{}'.format(node_info['ip'], service_port, endpoints['slice_view']), slice_view)


def terminate_slice(slice_id):
    sh = db_handler.SliceInfoHandler()
    if sh.get_slice_info(slice_id) is None and slice_manager_repo.check_im_existence(slice_id) is False:
        raise exception.PPSliceManagementError("Slice ID " + slice_id + "not exists")
    try:
        imanager = slice_manager_repo.get_instance_manager(slice_id)
        imanager.unxepose_all_slice_instances()
        imanager.remove_all_slice_deployments()
        sh.remove_slice_info(slice_id)  # NOTE: remove related the files also?
        service_multiport_handler.release_ports(slice_id)
        slice_manager_repo.remove_instance_manager(slice_id)

        fmanger = faas_manager_repo.get_faas_manager(slice_id)
        faas_api = configuration_handler.get_module_config('faas_api')
        fmanger.delete_package(url='https://{}:{}{}'.format(faas_api['faasIP'], faas_api['faasPort'], faas_api['base_url']))
    except exception.PPDbError as e:
        logger.info("Instance Manager not found - " + str(e))



def update_slice(slice_update_info):
    pass


"""
    Admin methods
"""


def set_new_kpi(kpi_info):
    db = db_handler.KpiHandler()
    try:
        # FIXME: Check if jsonschema is correct
        db.add_new_kpi(kpi_info)
    except exception.PPDbError as e:
        logger.error("DB ERROR - {}".format(str(e)))
        raise
    except exception.PPParserError as e:
        logger.error("ERROR in parsing KPI definition - {}".format(str(e)))
        raise


def rebuild_db():
    # load descriptors
    parser_conf = configuration_handler.get_module_config('parser')
    fh = db_handler.FileHandler(path=parser_conf['descriptor_path'])
    ch = db_handler.ComponentsHandler()
    ch.truncate_table('component')
    ch.truncate_table('component_kpi')
    ph = db_handler.PlugAndPlayControlHandler()
    ph.truncate_table('plugin')
    ph.truncate_table('plugin_kpi')
    sh = db_handler.SliceInfoHandler()
    sh.truncate_table('slice_file')
    descriptor_list = fh.load_descriptors_list()
    for file_descriptor in descriptor_list:
        parsed_element = parser.DescriptorParser.parse_file_descriptor(filename=file_descriptor,
                                                                       path=parser_conf['descriptor_path'])
        # NOTE Parsed element is a list [component/plugin, id, name, [[kpi_id, kpi_level, kpi_type], ...]]
        if parsed_element[0] == 'plugin':  # FIXME: DB module need a refactor for uniforming names
            ph.add_new_plugin(parsed_element, file_descriptor)
        else:
            ch.add_new_component(parsed_element, file_descriptor)


def set_configuration(config_info):
    db = db_handler.FileHandler()
    db.set_global_manager_conf(config_info)


def get_persistent_data(what):  # full, kpi, plugins, components, slice
    db = db_handler.DBHandler()
    try:
        if what == 'get-db':
            return db.get_full_db()
    except exception.PPDbError as e:
        logger.error("ERROR in retrieving DB stored data - " + str(e))
        raise


if __name__ == '__main__':
    # create_slice("nxw-slice", "cosa-a-caso")
    # time.sleep(10)
    # terminate_slice("nxw-slice")
    rebuild_db()
