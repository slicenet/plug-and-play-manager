#  Copyright (c)  2019.  Nextworks s.r.l.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

import logging
import json

logging.basicConfig(filename="plug_and_play_manager.log", format='[%(name)s] %(asctime)s - %(message)s', filemode='w',
                    level=logging.DEBUG)


class PlugAndPlayManagerError(Exception):
    def __int__(self, reason, etype='GENERIC_ERROR'):
        self.reason = reason
        self.etype = etype


class PPMKUbernetesError(PlugAndPlayManagerError):
    def __init__(self, reason, etype="KUBERNETES_ERROR"):
        super().__init__(reason, etype)


class PPMNetworkError(PlugAndPlayManagerError):
    def __init__(self, reason, etype="NETWORK_ERROR"):
        super().__init__(reason, etype)


class PPSliceManagementError(PlugAndPlayManagerError):
    def __init__(self, reason, etype="SLICE_MANAGEMENT_ERROR"):
        super().__init__(reason, etype)


class PPDbError(PlugAndPlayManagerError):
    def __init__(self, reason, etype="DATABASE_ERROR"):
        super().__init__(reason, etype)


class PPParserError(PlugAndPlayManagerError):
    def __init__(self, reason, etype="PARSER_ERROR"):
        super().__init__(reason, etype)


class PPConfigurationError(PlugAndPlayManagerError):
    def __init__(self, reason, etype="CONFIGURATION_ERROR"):
        super().__init__(reason, etype)


def kube_exception_parser(exct_body):
    e_dict = json.loads(exct_body.__dict__['body'])
    return e_dict['reason'], e_dict['message']
