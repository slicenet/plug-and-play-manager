#  Copyright (c)  2019.  Nextworks s.r.l.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#


import yaml
import json


class KubeConfigGenerator:
    def __init__(self, yaml_save_path, json_save_path, base_deployment_model, base_service_model):
        self.yaml_save_path = yaml_save_path
        self.json_save_path = json_save_path
        self.base_deployment_model = base_deployment_model
        self.base_service_model = base_service_model

    def _pretty_return(self, data, file):
        return {"data": data, "file": file}

    def _generate_cpsr_config(self, slice_id, cpsr_ip):

        config = dict()
        config['env'] = []
        config['env'].append({'name': "cpsrIP", "value": cpsr_ip})
        config['env'].append({'name': "slicenetId", "value": slice_id})
        config['env'].append({'name': "hostIP", "valueFrom": {"fieldRef": {"fieldPath": "status.hostIP"}}})
        return config

    def generate_deployment_yaml(self, name, container_info_list, container_type, cpsr_info=None, image_pull_secrets = None):
        with open(self.base_deployment_model, "r") as f:
            base_model = yaml.load(f)
        try:
            base_model['metadata']['name'] = name + "-" + container_type
            base_model['spec']['selector']['matchLabels']['app'] = name
            base_model['spec']['template']['metadata']['labels']['app'] = name
            if cpsr_info is not None:
                config = self._generate_cpsr_config(name, cpsr_info['cpsrIP'])
                for info in container_info_list:
                    info['env'] = []
                    info['env'] = config['env']
            for container in container_info_list:
                container['imagePullPolicy'] = 'IfNotPresent'
            base_model['spec']['template']['spec']['containers'] = container_info_list
            if image_pull_secrets is not None:
                base_model['spec']['template']['spec']['imagePullSecrets'] = []
                base_model['spec']['template']['spec']['imagePullSecrets'].append({'name': image_pull_secrets})

        except KeyError as e:
            print(e)

        with open(self.yaml_save_path + '{}_kube_deployment_{}.yaml'.format(name, container_type), 'w') as outfile:
            yaml.dump(base_model, outfile, default_flow_style=False)

        return self._pretty_return(base_model, outfile.name)

    def modify_component_deployment(self, name, component_deployment, ports):
        port_list = ports.copy()
        for container in component_deployment['spec']['template']['spec']['containers']:

            #container['ports'][0]['containerPort'] = port_list[0]
            #container['env'] = [{'name': "port", 'value': int(port_list[0])}] + container['env']
            container['env'].append({'name': "port", 'value': str(port_list[0])})
            port_list.pop(0)

        with open(self.yaml_save_path + '{}_kube_deployment_{}.yaml'.format(name, 'component'), 'w') as outfile:
            yaml.dump(component_deployment, outfile, default_flow_style=False)
        return component_deployment


    def get_deployment_model(self, name, container_type):
        with open(self.yaml_save_path + '{}_kube_deployment_{}.yaml'.format(name, container_type), "r") as f:
            return yaml.load(f)

    def generate_service_yaml(self, name, service_port):
        with open(self.base_service_model, "r") as f:
            base_model = yaml.load(f)
        try:
            base_model['metadata']['name'] = name
            base_model['metadata']['labels']['app'] = name
            base_model['spec']['ports'][0]['nodePort'] = service_port
            base_model['spec']['selector']['app'] = name

        except KeyError as e:
            print(e)

        with open(self.yaml_save_path + '{}_kube_service.yaml'.format(name), 'w') as outfile:
            yaml.dump(base_model, outfile, default_flow_style=False)
        return self._pretty_return(base_model, outfile.name)

    def generate_slice_service(self, name, core, components, ports):
        port_element = dict()
        with open(self.base_service_model, "r") as f:
            base_model = yaml.load(f)
            """spec:
                  type: NodePort
                  ports:
                  - name: http
                    protocol: TCP
                    port: 60002
                    nodePort:
                  selector:
                    app:"""
        # return {'name': descriptor['plugin_name'], 'image': descriptor['plugin_image'], 'ports': port_list}
        try:
            base_model['metadata']['name'] = name
            base_model['metadata']['labels']['app'] = name
            base_model['spec']['selector']['app'] = name
            if core:
                port_element['name'] = 'pp-core-port'
                port_element['protocol'] = 'TCP'
                port_element['nodePort'] = ports[0]
                base_model['spec']['ports'].append(port_element)
                ports.pop(0)

            for component in components:
                port_element['name'] = '{}-{}'.format(component['name'], 'port')
                port_element['protocol'] = 'TCP'
                port_element['port'] = component['ports'][0]['containerPort']
                port_element['nodePort'] = ports[0]
                base_model['spec']['ports'].append(port_element)
                ports.pop(0)

        except KeyError as e:
            print(e)

        with open(self.yaml_save_path + '{}_kube_service.yaml'.format(name), 'w') as outfile:
            yaml.dump(base_model, outfile, default_flow_style=False)
        return self._pretty_return(base_model, outfile.name)

    def generate_slice_view(self, slice_id, slice_info, plugin_set):

        with open(self.json_save_path + '{}-slice-view.json'.format(slice_id), "r") as f:

            slice_view = json.load(f)

        return slice_view

    def generate_repo_info(self, repo_ip, repo_port):
        return {"cpsr_ip": repo_ip, "cpsr_port": repo_port}


class SliceViewConfigGenerator:
    def __init__(self, slice_sso_path):
        self.path = slice_sso_path

    def generate_slice_view(self, slice_id, slice_info, plugin_set):
        topology = slice_info['domains']
        kpi_list = slice_info['required_kpi']
        # [key, kpi_level, kpi_type, element[key]]
        with open('{}slice_view_template.json'.format(self.path), "r") as f:
            slice_view_template = json.load(f)

        # prepare slice_view header
        header = slice_view_template['header']
        header['slice_id'] = slice_id
        header['tenant_id'] = slice_info['slice_tenant']
        if len(topology) > 1:
            header['domain_type'] = "multi"
        else:
            header['domain_type'] = "single"

        # prepare elements
        element = slice_view_template['element']

        for domain in topology:
            domain_id = domain["domain_id"]
            for e in domain['elements']:
                """"element": {
                    "element_id": "",
                      "type": "",
                      "level": "",
                      "family": "",
                      "location": "",
                      "domain_id": "",
                      "properties": []
                  }"""
                element['element_id'] = e['id']
                element['element_name'] = e['name']
                element['type'] = e['type']
                element['level'] = e['level']
                element['domain_id'] = domain_id
                for sn in e['kpis']:
                    k = kpi_list[int(sn)]
                    """"property": {
                        "type": "",
                          "property_id": "",
                          "family": "",
                          "port": "",
                          "allowed_api": []
                      }"""
                    prop = slice_view_template['property']
                    prop['type'] = k['id']

    def generate_repo_info(self, repo_ip, repo_port):
        return {"cpsr_ip": repo_ip, "cpsr_port": repo_port}


class FaasConfigGenerator:
    def __init__(self, base_package_model, base_action_model, base_trigger_model, base_trigger_reg_model, base_rule_model, code_file_path):
        self.base_package_model = base_package_model
        self.base_action_model = base_action_model
        self.code_file_path = code_file_path
        self.base_trigger_model = base_trigger_model
        self.base_trigger_reg_model = base_trigger_reg_model
        self.base_rule_model = base_rule_model

    def generate_package_json(self, namespace, package_name):
        with open(self.base_package_model, 'r') as pf:
            package = json.load(pf)

        package['name'] = package_name
        package['namespace'] = namespace

        return package

    def generate_action_json(self, namespace, action_name, code_file):
        with open(self.base_action_model, 'r') as af:
            action = json.load(af)
        with open("{}{}".format(self.code_file_path, code_file), 'r') as cf:
            code = cf.read()

        action['exec']['code'] = code
        action['name'] = action_name
        action['namespace'] = namespace

        return action

    def generate_trigger_json(self, namespace, trigger_name):
        with open(self.base_trigger_model, 'r') as tf:
            trigger = json.load(tf)
        trigger['name'] = trigger_name
        trigger['annotations'].append({"key": "feed", "value": "/whisk.system/messaging/kafkaFeed"})
        trigger['namespace'] = namespace
        return trigger

    def register_trigger_json(self, authkey, brokers, topic, trigger_name):
        with open(self.base_trigger_reg_model, 'r') as trf:
            trigger_reg = json.load(trf)

        trigger_reg['authKey'] = authkey
        trigger_reg['brokers'] = brokers
        trigger_reg['topic'] = topic
        trigger_reg['triggerName'] = trigger_name  # should include trigger_anamespace

        return trigger_reg

    def generate_rule_json(self, name, trigger_name, action_name, namespace):
        with open(self.base_trigger_reg_model, 'r') as rf:
            rule = json.load(rf)

        rule['name'] = name
        rule['trigger'] = '/{}/{}'.format(namespace, trigger_name)
        rule['action'] = '/{}/{}'.format(namespace, action_name)

        return rule


def main():
    pass


if __name__ == '__main__':
    main()
