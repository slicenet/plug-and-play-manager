#  Copyright (c)  2019.  Nextworks s.r.l.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

import requests
import logging
import json
from urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter
from manager import exception

module_name = "P&P_MANAGER_BASE_REST_CLIENT"
logger = logging.getLogger(module_name)


def retry_session(retries, session=None, backoff_factor=0.3, status_forcelist=(500, 502, 503, 504)):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


class BaseRestClient:

    def get(self, url, headers=None,  user=None, password=None, params=None):

        session = requests.Session()
        session = retry_session(retries=10, session=session)
        if user is not None and password is not None:
            # auth = session.auth.HTTPBasicAuth(user, password)
            session.auth = (user, password)

        try:
            logger.info('Executing GET to: ' + url)
            http_response = session.request("GET", url, headers=headers, params=params, verify=False)
        except requests.exceptions.RequestException as e:
            logger.error("Connection Error " + str(e))
            raise exception.PPMNetworkError(reason=str(e), etype="REQUEST_FAILURE")

        if not 199 < http_response.status_code < 300:
            logger.error("error in http_response: " + str(http_response.status_code))
            raise exception.PPMNetworkError(reason="Error in HTTP Response: " + str(http_response.status_code),
                                            etype="HTTP_RESPONSE_ERROR")

        return http_response

    def post(self, url, data=None, headers=None, user=None, password=None):

        session = requests.Session()
        session = retry_session(retries=10, session=session)

        if user is not None and password is not None:
            session.auth = (user, password)
        try:
            logger.info('Executing POST to: ' + url)
            http_response = session.post(url, data, headers=headers, verify=False)
        except requests.exceptions.RequestException as e:
            logger.error("Connection Error " + str(e))
            raise exception.PPMNetworkError(reason=str(e), etype="REQUEST_FAILURE")
        if not 199 < http_response.status_code < 300:
            logger.error("error in http_response: " + str(http_response.status_code))
            raise exception.PPMNetworkError(reason="Error in HTTP Response: " + str(http_response.status_code),
                                            etype="HTTP_RESPONSE_ERROR")
        return http_response

    def put(self, url, data=None, headers=None, user=None, password=None):

        session = requests.Session()
        session = retry_session(retries=10, session=session)

        if user is not None and password is not None:
            session.auth = (user, password)
        try:
            logger.info('Executing PUT to: ' + url)
            http_response = session.put(url, data, headers=headers, verify=False)
        except requests.exceptions.RequestException as e:
            logger.error("Connection Error " + str(e))
            raise exception.PPMNetworkError(reason=str(e), etype="REQUEST_FAILURE")
        if not 199 < http_response.status_code < 300:
            logger.error("error in http_response: " + str(http_response.status_code))
            raise exception.PPMNetworkError(reason="Error in HTTP Response: " + str(http_response.status_code),
                                            etype="HTTP_RESPONSE_ERROR")
        return http_response

    def delete(self, url, user=None, password=None):

        session = requests.Session()
        session = retry_session(retries=10, session=session)

        if user is not None and password is not None:
            session.auth = (user, password)
        try:
            logger.info('Executing DELETE to: ' + url)
            http_response = session.delete(url=url, verify=False)
        except requests.exceptions.RequestException as e:
            logger.error("Connection Error " + str(e))
            raise exception.PPMNetworkError(reason=str(e), etype="REQUEST_FAILURE")
        if not 199 < http_response.status_code < 300:
            logger.error("error in http_response: " + str(http_response.status_code))
            raise exception.PPMNetworkError(reason="Error in HTTP Response: " + str(http_response.status_code),
                                            etype="HTTP_RESPONSE_ERROR")
        return http_response


if __name__ == '__main__':
    rc = BaseRestClient()
    """payload = {"cpsr_ip": '10.0.8.28', "cpsr_port": '8080'}
    url = "http://10.0.8.28:30001/plug-and-play-test/pp_management/registration/cpsr-info/"
    print(rc.post(url, json.dumps(payload)))"""

    print(rc.get("http://www.google.it").text)
