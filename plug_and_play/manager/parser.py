#  Copyright (c)  2019.  Nextworks s.r.l.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#


import json
import logging

module_name = "P&P_MANAGER_PARSER"
logger = logging.getLogger(module_name)

# NBI request parser


class NbiParser:
    @staticmethod
    def extract_slice_kpis(slice_info):
        return slice_info['required_kpi']

    @staticmethod
    def extract_slice_topology(slice_info):
        return slice_info['domains']


class DescriptorParser:

    # Note: descriptor_info is a tuple --> descriptor_info[2] == descriptor_file_name
    @staticmethod
    def parse_plugin_descriptor(descriptor_info, path='manager/manager_persistence/descriptor_catalogue/'):
        with open(path+descriptor_info[2], 'r') as f:  # descriptor_info[2] is the descriptor filename
            descriptor = json.load(f)

        # FIXME: exception handling here
        # Check retrieved values
        if descriptor['plugin_id'] != descriptor_info[0] or descriptor['plugin_name'] != descriptor_info[1]:
            pass  # raise exception
        port_list = []
        for port in descriptor['plugin_ports']:
            port_list.append({'containerPort': int(port)})
        return {'name': descriptor['plugin_name'], 'image': descriptor['plugin_image'], 'ports': port_list}

    @staticmethod
    def parse_component_descriptor(descriptor_info, path='manager/manager_persistence/descriptor_catalogue/'):
        with open(path + descriptor_info[2], 'r') as f:  # descriptor_info[2] is the descriptor filename
            descriptor = json.load(f)

        # FIXME: exception handling here
        # Check retrieved values
        if descriptor['component_id'] != descriptor_info[0] or descriptor['component_name'] != descriptor_info[1]:
            pass  # raise exception
        port_list = []
        for port in descriptor['component_ports']:
            port_list.append({'containerPort': int(port)})
        return {'name': descriptor['component_name'], 'image': descriptor['component_image'], 'ports': port_list}

    @staticmethod
    def assemble_element_set(descriptors_info, core_file_name='core_control_descriptor.json',
                             path='manager/manager_persistence/descriptor_catalogue/'):
        element_set = list()
        #  retrieve core
        # fixme exception handling here
        if core_file_name is not None:
            element_set.append(DescriptorParser.parse_plugin_descriptor(('core-container', 'PP_core_control',
                                                                         core_file_name), path=path))
            for item in descriptors_info:
                element_set.append(DescriptorParser.parse_plugin_descriptor(item, path))

        else:
            for item in descriptors_info:
                element_set.append(DescriptorParser.parse_component_descriptor(item, path))

        return element_set

    @staticmethod
    def parse_file_descriptor(filename, path='manager/manager_persistence/descriptor_catalogue/'):
        with open('{}{}'.format(path, filename), "r") as f:
            descriptor = json.load(f)

        try:
            keyword = descriptor['type']
            identifier = descriptor["{}_id".format(keyword)]
            name = descriptor["{}_name".format(keyword)]

            capabilities = descriptor['capabilities']
            res_list = list()
            for kpi_type in capabilities.keys():
                for kpi_level in capabilities[kpi_type].keys():
                    for element in capabilities[kpi_type][kpi_level]:
                        for key in element.keys():
                            res_list.append([key, kpi_level, kpi_type, element[key]])

            return [keyword, identifier, name, res_list]
        except KeyError as e:
            logger.error("ERROR in parsing descriptor - {}".format(str(e)))
            raise

    @staticmethod
    def plugin_info_for_slice_view(descriptor_info, kpi_triplets,
                                   path='manager/manager_persistence/descriptor_catalogue/'):
        with open(path + descriptor_info[2], 'r') as f:  # descriptor_info[2] is the descriptor filename
            descriptor = json.load(f)

        # assemble info
        try:
            identifier = descriptor['plugin_id']
            port = descriptor['plugin_port'][0]  # plugins have only one tcp port @NBI
            # kpi_triplet = [[kpi_id, kpi_level, kpi_type], ..., ]
            api_list = list()
            for element in kpi_triplets:
                for kpi_dict in descriptor['capabilities'][element[1]]:
                    if element[0] in kpi_dict:
                        api_list.append(kpi_dict[element[0]])
            return [identifier, port, api_list]
        except KeyError as e:
            logger.error("ERROR in parsing descriptor - {}".format(str(e)))
            raise
        

"""   DELETE DEPLOYMENT/SERVICE
{'api_version': 'v1',
 'code': None,
 'details': {'causes': None,
             'group': 'apps',
             'kind': 'deployments',
             'name': 'plug-play-slice-nxw',
             'retry_after_seconds': None,
             'uid': '10654262-02dc-11e9-8cae-fa163e3d9ba6'},
 'kind': 'Status',
 'message': None,
 'metadata': {'_continue': None, 'resource_version': None, 'self_link': None},
 'reason': None,
 'status': 'Success'}
"""


def parse_delete_reply(kind, json_reply):
    try:
        if json_reply.details.kind == kind and json_reply.status == 'Success':
            return True
        return False
    except KeyError as e:
        logger.error("Error in parsing " + kind + " delete reply - " + str(e))
        raise


"""
{'api_version': 'apps/v1',
 'kind': 'Deployment',
 'metadata': {'annotations': None,
              'cluster_name': None,
              'creation_timestamp': datetime.datetime(2018, 12, 19, 16, 31, 58, tzinfo=tzutc()),
              'deletion_grace_period_seconds': None,
              'deletion_timestamp': None,
              'finalizers': None,
              'generate_name': None,
              'generation': 1,
              'initializers': None,
              'labels': None,
              'name': 'plug-play-slice-nxw',
              'namespace': 'default',
              'owner_references': None,
              'resource_version': '5902868',
              'self_link': '/apis/apps/v1/namespaces/default/deployments/plug-play-slice-nxw',
              'uid': '9ae0a64e-03ab-11e9-8cae-fa163e3d9ba6'},
 'spec': {'min_ready_seconds': None,
          'paused': None,
          'progress_deadline_seconds': 600,
          'replicas': 1,
          'revision_history_limit': 10,
          'selector': {'match_expressions': None,
                       'match_labels': {'app': 'plug-play-slice-nxw',
                                        'tier': 'frontend'}},
          'strategy': {'rolling_update': {'max_surge': '25%',
                                          'max_unavailable': '25%'},
                       'type': 'RollingUpdate'},
          'template': {'metadata': {'annotations': None,
                                    'cluster_name': None,
                                    'creation_timestamp': None,
                                    'deletion_grace_period_seconds': None,
                                    'deletion_timestamp': None,
                                    'finalizers': None,
                                    'generate_name': None,
                                    'generation': None,
                                    'initializers': None,
                                    'labels': {'app': 'plug-play-slice-nxw',
                                               'tier': 'frontend'},
                                    'name': None,
                                    'namespace': None,
                                    'owner_references': None,
                                    'resource_version': None,
                                    'self_link': None,
                                    'uid': None},
                       'spec': {'active_deadline_seconds': None,
                                'affinity': None,
                                'automount_service_account_token': None,
                                'containers': [{'args': None,
                                                'command': None,
                                                'env': None,
                                                'env_from': None,
                                                'image': 'core:v1.1.0',
                                                'image_pull_policy': 'IfNotPresent',
                                                'lifecycle': None,
                                                'liveness_probe': None,
                                                'name': 'core-container',
                                                'ports': [{'container_port': 60001,
                                                           'host_ip': None,
                                                           'host_port': None,
                                                           'name': None,
                                                           'protocol': 'TCP'},
                                                          {'container_port': 60002,
                                                           'host_ip': None,
                                                           'host_port': None,
                                                           'name': None,
                                                           'protocol': 'TCP'}],
                                                'readiness_probe': None,
                                                'resources': {'limits': None,
                                                              'requests': None},
                                                'security_context': None,
                                                'stdin': None,
                                                'stdin_once': None,
                                                'termination_message_path': '/dev/termination-log',
                                                'termination_message_policy': 'File',
                                                'tty': None,
                                                'volume_devices': None,
                                                'volume_mounts': None,
                                                'working_dir': None},
                                               {'args': None,
                                                'command': None,
                                                'env': None,
                                                'env_from': None,
                                                'image': 'plugin2:v5.0.3',
                                                'image_pull_policy': 'IfNotPresent',
                                                'lifecycle': None,
                                                'liveness_probe': None,
                                                'name': 'plugin2-container',
                                                'ports': [{'container_port': 65002,
                                                           'host_ip': None,
                                                           'host_port': None,
                                                           'name': None,
                                                           'protocol': 'TCP'}],
                                                'readiness_probe': None,
                                                'resources': {'limits': None,
                                                              'requests': None},
                                                'security_context': None,
                                                'stdin': None,
                                                'stdin_once': None,
                                                'termination_message_path': '/dev/termination-log',
                                                'termination_message_policy': 'File',
                                                'tty': None,
                                                'volume_devices': None,
                                                'volume_mounts': None,
                                                'working_dir': None},
                                               {'args': None,
                                                'command': None,
                                                'env': None,
                                                'env_from': None,
                                                'image': 'plugin3:v5.0.3',
                                                'image_pull_policy': 'IfNotPresent',
                                                'lifecycle': None,
                                                'liveness_probe': None,
                                                'name': 'plugin3-container',
                                                'ports': [{'container_port': 65003,
                                                           'host_ip': None,
                                                           'host_port': None,
                                                           'name': None,
                                                           'protocol': 'TCP'}],
                                                'readiness_probe': None,
                                                'resources': {'limits': None,
                                                              'requests': None},
                                                'security_context': None,
                                                'stdin': None,
                                                'stdin_once': None,
                                                'termination_message_path': '/dev/termination-log',
                                                'termination_message_policy': 'File',
                                                'tty': None,
                                                'volume_devices': None,
                                                'volume_mounts': None,
                                                'working_dir': None},
                                               {'args': None,
                                                'command': None,
                                                'env': None,
                                                'env_from': None,
                                                'image': 'plugin4:v5.0.3',
                                                'image_pull_policy': 'IfNotPresent',
                                                'lifecycle': None,
                                                'liveness_probe': None,
                                                'name': 'plugin4-container',
                                                'ports': [{'container_port': 65004,
                                                           'host_ip': None,
                                                           'host_port': None,
                                                           'name': None,
                                                           'protocol': 'TCP'}],
                                                'readiness_probe': None,
                                                'resources': {'limits': None,
                                                              'requests': None},
                                                'security_context': None,
                                                'stdin': None,
                                                'stdin_once': None,
                                                'termination_message_path': '/dev/termination-log',
                                                'termination_message_policy': 'File',
                                                'tty': None,
                                                'volume_devices': None,
                                                'volume_mounts': None,
                                                'working_dir': None}],
                                'dns_config': None,
                                'dns_policy': 'ClusterFirst',
                                'host_aliases': None,
                                'host_ipc': None,
                                'host_network': None,
                                'host_pid': None,
                                'hostname': None,
                                'image_pull_secrets': None,
                                'init_containers': None,
                                'node_name': None,
                                'node_selector': None,
                                'priority': None,
                                'priority_class_name': None,
                                'restart_policy': 'Always',
                                'scheduler_name': 'default-scheduler',
                                'security_context': {'fs_group': None,
                                                     'run_as_non_root': None,
                                                     'run_as_user': None,
                                                     'se_linux_options': None,
                                                     'supplemental_groups': None},
                                'service_account': None,
                                'service_account_name': None,
                                'subdomain': None,
                                'termination_grace_period_seconds': 30,
                                'tolerations': None,
                                'volumes': None}}},
 'status': {'available_replicas': None,
            'collision_count': None,
            'conditions': None,
            'observed_generation': None,
            'ready_replicas': None,
            'replicas': None,
            'unavailable_replicas': None,
            'updated_replicas': None}}
"""


def parse_creation_reply(kind, json_reply):
    try:
        if json_reply.kind == kind:
            return {'name': json_reply.metadata.name, 'uid': json_reply.metadata.uid}
    except KeyError as e:
        logger.error("Error in parsing " + kind + " delete reply - " + str(e))
        raise


"""
{'api_version': 'v1',
 'kind': 'Service',
 'metadata': {'annotations': None,
              'cluster_name': None,
              'creation_timestamp': datetime.datetime(2018, 12, 19, 16, 34, 29, tzinfo=tzutc()),
              'deletion_grace_period_seconds': None,
              'deletion_timestamp': None,
              'finalizers': None,
              'generate_name': None,
              'generation': None,
              'initializers': None,
              'labels': {'app': 'plug-play-slice-nxw', 'tier': 'frontend'},
              'name': 'plug-play-slice-nxw',
              'namespace': 'default',
              'owner_references': None,
              'resource_version': '5903084',
              'self_link': '/api/v1/namespaces/default/services/plug-play-slice-nxw',
              'uid': 'f51a9b56-03ab-11e9-8cae-fa163e3d9ba6'},
 'spec': {'cluster_ip': '10.111.35.220',
          'external_i_ps': None,
          'external_name': None,
          'external_traffic_policy': 'Cluster',
          'health_check_node_port': None,
          'load_balancer_ip': None,
          'load_balancer_source_ranges': None,
          'ports': [{'name': 'http',
                     'node_port': 30002,
                     'port': 60002,
                     'protocol': 'TCP',
                     'target_port': 60002}],
          'publish_not_ready_addresses': None,
          'selector': {'app': 'plug-play-slice-nxw', 'tier': 'frontend'},
          'session_affinity': 'None',
          'session_affinity_config': None,
          'type': 'NodePort'},
 'status': {'load_balancer': {'ingress': None}}}
"""