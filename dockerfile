FROM python:3.6-alpine
RUN apk update && apk add git
RUN pip install tornado
RUN pip install requests
RUN pip install pyaml
RUN pip install git+https://github.com/kubernetes-client/python.git
COPY models models
COPY plug_and_play plug_and_play
COPY test test
RUN pwd
EXPOSE 50001
EXPOSE 50002
RUN ls -l
WORKDIR /plug_and_play
CMD python3 pp_manager.py
