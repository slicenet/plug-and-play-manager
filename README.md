## Plug and Play Manager
Life-cycle manager for slice application and control functions in SliceNet

### Overview
The manager currently supports the life-cycle management (creation-configuration-update-terminate)
of the following compoenents in the SliceNet architecture:

- Plug and Play Control framework:
    - Core Control
    - Plugins
- QoE Optimizer
- Control Plane Services (CPS)

FaaS with [OpenWhisk] (https://openwhisk.apache.org/) if supported for specific purposes
(i.e. fire the function execution on a specific message on Kafka Bus).
Features supported:
- Action (function): 
    - creation/termination
- Trigger:
    - creation/termination/registration on Kafka bus
- Rule 
    - creation/termination
    
### Usage
The software is written in Python (3.6+) and can be directly executed. However,
as dependences from external packages exists, an encapsulation in a Docker container
is strongly recommended. For this purpose, a dockerfile is provided.      
